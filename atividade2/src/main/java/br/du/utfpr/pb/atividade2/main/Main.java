/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.du.utfpr.pb.atividade2.main;

import br.edu.utfpr.pb.atividade2.dao.CidadeDao;
import br.edu.utfpr.pb.atividade2.dao.ClientesDao;
import br.edu.utfpr.pb.atividade2.dao.EstadoDao;
import br.edu.utfpr.pb.atividade2.dao.FornecedoresDao;
import br.edu.utfpr.pb.atividade2.dao.FuncionariosDao;
import br.edu.utfpr.pb.atividade2.dao.MovimentacaoDao;
import br.edu.utfpr.pb.atividade2.dao.OSDao;
import br.edu.utfpr.pb.atividade2.dao.ProdutosDao;
import br.edu.utfpr.pb.atividade2.dao.TelefonesDao;
import br.edu.utfpr.pb.atividade2.dao.TipoOperacaoDao;
import br.edu.utfpr.pb.atividade2.dao.TipoProdutoDao;
import br.edu.utfpr.pb.atividade2.dao.TipoServicoDao;
import br.edu.utfpr.pb.atividade2.model.Cidade;
import br.edu.utfpr.pb.atividade2.model.Clientes;
import br.edu.utfpr.pb.atividade2.model.Estado;
import br.edu.utfpr.pb.atividade2.model.Fornecedores;
import br.edu.utfpr.pb.atividade2.model.Funcionarios;
import br.edu.utfpr.pb.atividade2.model.Produtos;
import br.edu.utfpr.pb.atividade2.model.Telefones;
import br.edu.utfpr.pb.atividade2.model.TipoProduto;

/**
 *
 * @author gustavo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EstadoDao estadoDao = new EstadoDao();
        CidadeDao cidadeDao = new CidadeDao();
        FuncionariosDao funcionariosDao = new FuncionariosDao();
        ClientesDao clientesDao = new ClientesDao();
        FornecedoresDao fornecedoresDao = new FornecedoresDao();
        TelefonesDao telefonesDao = new TelefonesDao();
        TipoProdutoDao tipoProdutoDao = new TipoProdutoDao();
        ProdutosDao produtosDao = new ProdutosDao();
        TipoOperacaoDao tipoOperacaoDao = new TipoOperacaoDao();
        OSDao osDao = new OSDao();
        MovimentacaoDao movimentacaoDao = new MovimentacaoDao();
        TipoServicoDao tipoServicoDao = new TipoServicoDao();

        Estado estado = new Estado();
        estado.setNome("Tocantins");

        estadoDao.insert(estado);

        Cidade cidade = new Cidade();
        cidade.setNome("Palmas");
        cidade.setEstado(estado);

        cidadeDao.insert(cidade);

        Funcionarios funcionarios = new Funcionarios();
        funcionarios.setNome("Fulano");
        funcionarios.setCpf(1L);
        funcionarios.setLogradouro("Rua Teste, 345");
        funcionarios.setBairro("Teste");
        funcionarios.setComplemento("Bloco 1");
        funcionarios.setCidade(cidade);
        funcionarios.setCep(1L);

        funcionariosDao.insert(funcionarios);

        Clientes clientes = new Clientes();
        clientes.setNome("Ciclano");
        clientes.setCpf(1L);
        clientes.setLogradouro("Rua Teste, 123");
        clientes.setBairro("Teste");
        clientes.setComplemento("Bloco 2");
        clientes.setCidade(cidade);
        clientes.setCep(1L);

        clientesDao.insert(clientes);

        Fornecedores fornecedores = new Fornecedores();
        fornecedores.setNome("Beltrano");
        fornecedores.setCnpj(1L);
        fornecedores.setLogradouro("Rua Teste 567");
        fornecedores.setBairro("Teste");
        fornecedores.setComplemento("Galpão 22");
        fornecedores.setCidade(cidade);
        fornecedores.setCep(1L);

        fornecedoresDao.insert(fornecedores);

        Telefones telefones = new Telefones();
        telefones.setClientes(clientes);
        telefones.setDDD(46);
        telefones.setTelefone(1L);

        telefonesDao.insert(telefones);

        TipoProduto tipoProduto = new TipoProduto();
        tipoProduto.setNome("Periféricos");

        tipoProdutoDao.insert(tipoProduto);

        Produtos produtos = new Produtos();
        produtos.setNome("Teclado");
        produtos.setPreco(1D);
        produtos.setTipoProduto(tipoProduto);

        System.out.println(estado);
        System.out.println("\n Lista de Estados *******");
        estadoDao.findAll().forEach(e -> System.out.println(e));

        System.out.println(cidade);
        System.out.println("\n Lista de Cidades *******");
        cidadeDao.findAll().forEach(c -> System.out.println(c));

        System.out.println(funcionarios);
        System.out.println("\n Lista de Funcionários******");
        funcionariosDao.findAll().forEach(f -> System.out.println(f));

        System.out.println(clientes);
        System.out.println("\n Lista de Clientes ******");
        clientesDao.findAll().forEach(cl -> System.out.println(cl));

        System.out.println(fornecedores);
        System.out.println("\nLista de Fornecedores ******");
        fornecedoresDao.findAll().forEach(f -> System.out.println(f));

        System.out.println(telefones);
        System.out.println("\nLista de Telefones *******");
        telefonesDao.findAll().forEach(t -> System.out.println(t));

        System.out.println(tipoProduto);
        System.out.println("\nLista de Tipos de Produtos*****");
        tipoProdutoDao.findAll().forEach(tp -> System.out.println(tp));

//        System.out.println(produtos);
//        System.out.println("\nLista de Produtos*****");
//        produtosDao.findAll().forEach(p -> System.out.println(p));

        System.out.println(produtos);
        System.out.println("****** TESTE findByNomeLike() ******");
        produtosDao.findByNomeLike("Teclado").forEach(p -> System.out.println(p));

    }

}
