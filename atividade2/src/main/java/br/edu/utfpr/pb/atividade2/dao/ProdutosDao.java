package br.edu.utfpr.pb.atividade2.dao;

import br.edu.utfpr.pb.atividade2.model.Produtos;
import java.util.List;
import javax.persistence.Query;

public class ProdutosDao extends GenericDao <Produtos, Long> {

    public ProdutosDao() {
        
        super(Produtos.class);
    
    }
    
    public List<Produtos> findByNomeLike(String nome){
        Query query = em.createQuery("SELECT p FROM Produtos p "
        + "WHERE p.nome LIKE :nome ORDER BY p.nome");
        
        query.setParameter("nome", "%" + nome + "%");
        return query.getResultList();
    }
    
     public List <Produtos> findByCategoriaId(Integer tipoProdutoId){
        Query query = em.createQuery("SELECT p FROM Produto p "
         + " WHERE p.tipoproduto.id = :tipoProdutoId");
        
        query.setParameter("tipoProdutoId", tipoProdutoId);
        
        return query.getResultList();
     }
}
