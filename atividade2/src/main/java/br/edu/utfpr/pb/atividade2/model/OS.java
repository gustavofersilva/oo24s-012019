package br.edu.utfpr.pb.atividade2.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "os")
public class OS implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer num;
    
    @Column (name = "data", nullable = false)
    private LocalDate data;
    
    @ManyToOne
    @JoinColumn (name = "funcionario_id", referencedColumnName = "id")
    private Funcionarios funcionario;
    
    @ManyToOne
    @JoinColumn (name = "cliente_id", referencedColumnName = "id")
    private Clientes cliente;
    
    @ManyToOne
    @JoinColumn (name = "servico_id", referencedColumnName = "id")
    private TipoServico tipoServico;
    
    @Column(name = "total", nullable = false)
    private Double total;
    
    @Column(name = "descricao", nullable = false, length = 100)
    private String descricao;

    public OS() {
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public TipoServico getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.num);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OS other = (OS) obj;
        if (!Objects.equals(this.num, other.num)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OS{" + "num=" + num + ", data=" + data + ", funcionario=" + funcionario + ", cliente=" + cliente + ", tipoServico=" + tipoServico + ", total=" + total + ", descricao=" + descricao + '}';
    }
    
    
    
}
