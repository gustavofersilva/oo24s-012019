package br.edu.utfpr.pb.atividade2.dao;

import br.edu.utfpr.pb.atividade2.model.TipoServico;


public class TipoServicoDao extends GenericDao <TipoServico, Integer> {

    public TipoServicoDao() {
        super(TipoServico.class);
    }
    
    
    
}
