package br.edu.utfpr.pb.atividade2.dao;

import br.edu.utfpr.pb.atividade2.model.TipoProduto;


public class TipoProdutoDao extends GenericDao <TipoProduto, Integer> {

    public TipoProdutoDao() {
        super(TipoProduto.class);
    }
    
    
    
}
