/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.pb.atividade2.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author gustavo
 */
public class Movimentacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "cod_tipo_op", referencedColumnName = "id")
    private TipoOperacao tipoOperacao;

    @ManyToOne
    @JoinColumn(name = "cod_funcionario", referencedColumnName = "id")
    private Funcionarios funcionario;

    @ManyToOne
    @JoinColumn(name = "cod_cliente", referencedColumnName = "id")
    private Clientes cliente;

    @ManyToOne
    @JoinColumn(name = "cod_produto", referencedColumnName = "id")
    private Produtos produto;

    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;

    @Column(name = "estoqueAtual", nullable = false)
    private Integer estoqueAtual;

    @Column(name = "estoqueAnterior", nullable = false)
    private Integer estoqueAnterior;

    @ManyToOne
    @JoinColumn(name = "num", referencedColumnName = "id")
    private OS num;

    @Column (name = "valorUn", nullable = false)
    private Double valorUn;

    public Movimentacao() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoOperacao getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(TipoOperacao tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Produtos getProduto() {
        return produto;
    }

    public void setProduto(Produtos produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getEstoqueAtual() {
        return estoqueAtual;
    }

    public void setEstoqueAtual(Integer estoqueAtual) {
        this.estoqueAtual = estoqueAtual;
    }

    public Integer getEstoqueAnterior() {
        return estoqueAnterior;
    }

    public void setEstoqueAnterior(Integer estoqueAnterior) {
        this.estoqueAnterior = estoqueAnterior;
    }

    public OS getNum() {
        return num;
    }

    public void setNum(OS num) {
        this.num = num;
    }

    public Double getValorUn() {
        return valorUn;
    }

    public void setValorUn(Double valorUn) {
        this.valorUn = valorUn;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movimentacao other = (Movimentacao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Movimentacao{" + "id=" + id + ", tipoOperacao=" + tipoOperacao + ", funcionario=" + funcionario + ", cliente=" + cliente + ", produto=" + produto + ", quantidade=" + quantidade + ", estoqueAtual=" + estoqueAtual + ", estoqueAnterior=" + estoqueAnterior + ", num=" + num + ", valorUn=" + valorUn + '}';
    }

    
    
}
