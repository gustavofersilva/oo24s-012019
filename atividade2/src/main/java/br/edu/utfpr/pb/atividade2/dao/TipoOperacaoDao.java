package br.edu.utfpr.pb.atividade2.dao;

import br.edu.utfpr.pb.atividade2.model.TipoOperacao;

/**
 *
 * @author gustavo
 */
public class TipoOperacaoDao extends GenericDao <TipoOperacao, Integer> {

    public TipoOperacaoDao() {
    super(TipoOperacao.class);
    }
    
}
