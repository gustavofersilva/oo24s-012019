package br.edu.utfpr.pb.atividade2.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "telefones")
public class Telefones implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Integer id;
    
    @ManyToOne
    @JoinColumn (name = "clientes_id", referencedColumnName = "id")
    private Clientes clientes;
    
    @ManyToOne
    @JoinColumn (name = "fornecedores_id", referencedColumnName = "id")
    private Fornecedores fornecedores;
    
    @ManyToOne
    @JoinColumn (name = "funcionarios_id", referencedColumnName = "id")
    private Funcionarios funcionarios;
    
    @Column (name = "ddd", nullable = false, length = 2)
    private Integer DDD;
    
    @Column (name = "telefone", nullable = false, length = 9)
    private Long telefone;

    public Telefones() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Fornecedores getFornecedores() {
        return fornecedores;
    }

    public void setFornecedores(Fornecedores fornecedores) {
        this.fornecedores = fornecedores;
    }

    public Funcionarios getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Funcionarios funcionarios) {
        this.funcionarios = funcionarios;
    }

    public Integer getDDD() {
        return DDD;
    }

    public void setDDD(Integer DDD) {
        this.DDD = DDD;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Telefones other = (Telefones) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Telefones{" + "id=" + id + ", clientes=" + clientes + ", fornecedores=" + fornecedores + ", funcionarios=" + funcionarios + ", DDD=" + DDD + ", telefone=" + telefone + '}';
    }
    
    
    
    
}
