package br.edu.utfpr.pb.oo24s.aula4.javafx.model;

public enum EOperadora {
    VIVO, TIM, OI, CLARO;
}
