package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.CategoriaDao;
import br.edu.utfpr.pb.aula2.model.Categoria;

public class MainAudited {

    
    public static void main(String[] args) {
        try {
          new MainAudited();
        } catch (Exception e) {
            e.printStackTrace();
        }
        

        
        System.exit(0);
    }

    public MainAudited() {
        testarCategorias();
    }

    private void testarCategorias() {
        CategoriaDao categoriaDao = new CategoriaDao();
        
        //insert Categoria 1
        Categoria c1 = new Categoria();
        c1.setDescricao ("desc.1 - cat. 1");
        categoriaDao.insert(c1);
        
        //insert Categoria 2
        Categoria c2 = new Categoria ();
        c2.setDescricao("desc. 1 - cat. 2");
        categoriaDao.insert(c2);
        
        // update categoria 1
        c1.setDescricao("desc. 2 - cat. 1");
        categoriaDao.update(c1);
        
       //update categoria 2
       c2.setDescricao("desc. 2 - cat. 2");
       categoriaDao.update(c2);
       
       //delete categoria 1       
       categoriaDao.delete(c1.getId());
        
        
    }
    
    
    
}
