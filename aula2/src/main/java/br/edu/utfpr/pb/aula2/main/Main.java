package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.CategoriaDao;
import br.edu.utfpr.pb.aula2.dao.ProdutoDao;
import br.edu.utfpr.pb.aula2.model.Categoria;
import br.edu.utfpr.pb.aula2.model.Produto;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {

        CategoriaDao categoriaDao = new CategoriaDao();

        Categoria categoria = new Categoria();
        categoria.setDescricao("Informática");

        categoriaDao.insert(categoria);

        categoria.setDescricao("Teste");
        categoriaDao.update(categoria);

        System.out.println(categoria);
        System.out.println("\n Lista de Categorias *******");
        categoriaDao.findAll().forEach(c -> System.out.println(c));

        Produto produto = new Produto();
        ProdutoDao produtoDao = new ProdutoDao();
        produto.setNome("Produto 1");
        produto.setDescricao("Descrição do Produto 1");
        produto.setValor(999D);
        //produto.setCategoria(categoria);
        produto.setCategoria(categoriaDao.findById(1));
        //  Categoria c1 = new Categoria();
        // c1.setId(1);
        // produto.setCategoria(c1);
        produtoDao.insert(produto);
        System.out.println(produto);
        System.out.println("****** TESTE findByNomeLike() ******");
        produtoDao.findByNomeLike("o").forEach(p -> System.out.println(p));
        
        produtoDao.findByNomeLike("o").forEach(p ->
            System.out.println(p));
        System.out.println("Total de produtos: " +
                produtoDao.count());
        produtoDao.findSQLNativo().forEach(
        
           obj -> System.out.println("Nome: " + obj [0] + 
                   " - Valor: " + obj [1]));
        
        System.exit(0);

    }


}
