package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.ClienteDao;
import br.edu.utfpr.pb.aula2.model.Cliente;
import br.edu.utfpr.pb.aula2.model.Contato;
import br.edu.utfpr.pb.aula2.model.EOperadora;
import br.edu.utfpr.pb.aula2.model.ETipoContato;
import java.util.ArrayList;
import java.util.List;

public class MainClienteContato {



    public static void main(String[] args) {
        MainClienteContato mc = new MainClienteContato();
        
        System.exit(0);
    }

    public MainClienteContato() {
        
        inserirCliente();
        
    }

    private void inserirCliente() {
        
        ClienteDao clienteDao = new ClienteDao();
        
        Cliente c1 = new Cliente();
        c1.setNome("João das Neves");
        c1.setCpf("12345678910");
        
        List<Contato> contatos = new ArrayList <>();
        Contato contato1 = new Contato();
        c1.setContatos(contatos);
        contato1.setCliente(c1);
        contato1.setTelefone("46 1234 5678");
        contato1.setOperadora(EOperadora.CLARO);
        contato1.setTipoContato(ETipoContato.CELULAR);
        contatos.add(contato1);
        
        Contato contato2 = new Contato ();
        contato2.setCliente(c1);
        contato2.setTelefone("46 1234 3322");
        contato2.setOperadora(EOperadora.OI);
        contato2.setTipoContato(ETipoContato.RESIDENCIAL);
        contatos.add(contato2);
        
        clienteDao.insert(c1);
        
    }
    
    
    
}
