package br.edu.utfpr.pb.aula2.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PJ")
public class PessoaJ extends Pessoa {
    
    @Column(name = "cnpj", length = 14)
    private String cnpj;
    
    @Column(name = "razao_social", length = 100)
    private String razaoSocial;
    
    @Column (name = "inscricao_estadual", length = 100)
    private String inscricaoEstadual;

    public PessoaJ() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }
    
    
    
    
}
