package br.edu.utfpr.pb.aula2.model;

public enum EOperadora {
    
    VIVO,
    TIM,
    OI,
    CLARO;
    
}
