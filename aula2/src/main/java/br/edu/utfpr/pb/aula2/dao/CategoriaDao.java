package br.edu.utfpr.pb.aula2.dao;

import br.edu.utfpr.pb.aula2.model.Categoria;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;


public class CategoriaDao extends GenericDao <Categoria, Integer>{
    
        
    public CategoriaDao(){
        super(Categoria.class);
    }    
    
    public Categoria getCategoriaLog (Integer id, int rev) throws Exception{
        try {
            AuditReader reader = AuditReaderFactory.get(em);
            
            Categoria categoria = reader.find(Categoria.class, id, rev);
            
            return categoria;
            
        } catch (Exception e) {
            
            throw new Exception ("Erro ao consultar log! " + e.getMessage());
        }
    }
    
    
}

    /*
    @PersistenceContext(unitName = "AulaHibernatePU")
    private EntityManager em;

    public CategoriaDao() {
        this.em = EntityManagerUtil.getEntityManager();
    }
    
    public void insert(Categoria categoria){
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.persist(categoria);
        em.flush();
        t.commit();
    }
    
    public void update(Categoria categoria){
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.merge(categoria);
        em.flush();
        t.commit();
    }
    
    public void delete(Integer id){
        Categoria categoria = findById(id);
        EntityTransaction t = em.getTransaction();
        t.begin();
        Categoria mergedEntity = em.merge(categoria);
        em.remove(mergedEntity);
        em.flush();
        t.commit();
    }

    public Categoria findById(Integer id) {
        return em.find(Categoria.class, id);
    }
    
    public List<Categoria> findAll(){
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Categoria> query = builder.createQuery(Categoria.class);
        query.from(Categoria.class);
        return em.createQuery(query).getResultList();
    }*/
   

