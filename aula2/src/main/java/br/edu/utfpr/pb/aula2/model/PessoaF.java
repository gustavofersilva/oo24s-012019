package br.edu.utfpr.pb.aula2.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PF")
public class PessoaF extends Pessoa {
    
    @Column(name = "cpf", length = 11)
    private String cpf;
    
    @Column(name = "data_nascimento")
    private LocalDate dataNascimento;

    public PessoaF() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    
    
    
}
