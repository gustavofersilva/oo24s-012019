package br.edu.utfpr.pb.aula2.dao;

import br.edu.utfpr.pb.aula2.model.Produto;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


public class ProdutoDao extends GenericDao<Produto, Long> {
    
    public ProdutoDao(){
        super(Produto.class);
    }    
    
    //SELECT p.* FROM produto AS p WHERE p.nome LIKE '%info%'
    public List<Produto> findByNomeLike(String nome){
        Query query = em.createQuery("SELECT p FROM Produto p "
        + "WHERE p.nome LIKE :nome ORDER BY p.nome");
        
        query.setParameter("nome", "%" + nome + "%");
        return query.getResultList();
    }
        //SELECT p.* FROM Produto as p WHERE p.categoria_id = 1 or 
        //p.descricao LIKE '%texto%'        
        public List <Produto> findByCategoriaIdOrDescricao(Integer categoriaId,
                String descricao){
        Query query = em.createQuery("SELECT p FROM Produto p "
         + " WHERE p.categoria.id = :categoriaId OR "
         + "p.descricao LIKE :descricao ORDER BY p.nome");
        
        query.setParameter("categoriaId", categoriaId);
        query.setParameter("descricao", "% + " + descricao + "%");
        
        return query.getResultList();
        
    }
        
    public List<Produto> findByValorPaginado(Double valor){
        Query query = em.createNamedQuery("SELECT p FROM Produto p "
                + "WHERE p.valor >= :valor");
        query.setParameter("valor", valor);
        query.setFirstResult(0);
        query.setMaxResults(2);
        return query.getResultList();
    }
    
    public Long count (){
        TypedQuery<Long> query = em.createQuery("SELECT count(p) FROM Produto p ", Long.class);
        
        return query.getSingleResult();
    }
    
    public List<Object[]> findSQLNativo(){
        Query query = em.createNativeQuery("SELECT p.nome, p.valor FROM produto as p");
        
        return query.getResultList();
    }
    
}
