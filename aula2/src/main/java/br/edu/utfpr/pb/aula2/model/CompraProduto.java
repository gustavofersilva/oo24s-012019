package br.edu.utfpr.pb.aula2.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "compra_produto")
public class CompraProduto implements Serializable{
    
    @EmbeddedId
    private CompraProdutoPK id;
    
    @Column (name = "quantidade", nullable = false)
    private Integer quantidade;
    
    @Column (name = "valor", nullable = false)
    private Double valor;

    public CompraProduto() {
    }
  
    public CompraProdutoPK getId() {
        return id;
    }

    public void setId(CompraProdutoPK id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraProduto other = (CompraProduto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CompraProduto{" + "id=" + id + ", quantidade=" + quantidade + ", valor=" + valor + '}';
    }
    
    
    
}
