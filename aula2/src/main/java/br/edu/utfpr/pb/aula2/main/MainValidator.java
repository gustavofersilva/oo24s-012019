package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.ProdutoDao;
import br.edu.utfpr.pb.aula2.model.Produto;

public class MainValidator {

    public static void main(String[] args) {
        
        new MainValidator();
        
        
        System.exit(0);
    }

    public MainValidator() {
        
        inserirProduto();
        
    }

    private void inserirProduto() {
        ProdutoDao produtoDao = new ProdutoDao();
        Produto p = new Produto();
        
        if (!produtoDao.isValid(p)){
            System.out.println(produtoDao.getErrors(p));
        }else{
            produtoDao.insert(p);
        }
    }
    
    
    
}
