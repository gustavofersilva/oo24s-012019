package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.PessoaDao;
import br.edu.utfpr.pb.aula2.model.PessoaF;
import br.edu.utfpr.pb.aula2.model.PessoaJ;
import java.time.LocalDate;

public class MainPessoa {

    public static void main(String[] args) {
        new MainPessoa();
        
        System.exit(0);
    }

    public MainPessoa() {
        inserirPessoa();
    }

    private void inserirPessoa() {
        PessoaDao pessoaDao = new PessoaDao();
        
        //Inserir pessoa física
        PessoaF pf = new PessoaF();
        pf.setNome("Pessoa física");
        pf.setCpf("12345678900");
        pf.setDataNascimento(LocalDate.of(2000,10,10));
        
        pessoaDao.insert(pf);
        
        //Inserir pessoa jurídica
        PessoaJ pj = new PessoaJ();
        pj.setNome("Pessoa Jurídica");
        pj.setRazaoSocial("Pj S/A");
        pj.setInscricaoEstadual("2233445566");
        pj.setCnpj("98765432199991");
        pessoaDao.insert(pj);
    }
    
    
}
