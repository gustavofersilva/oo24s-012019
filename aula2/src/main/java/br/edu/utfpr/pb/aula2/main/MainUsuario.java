package br.edu.utfpr.pb.aula2.main;

import br.edu.utfpr.pb.aula2.dao.CompraDao;
import br.edu.utfpr.pb.aula2.dao.PermissaoDao;
import br.edu.utfpr.pb.aula2.dao.ProdutoDao;
import br.edu.utfpr.pb.aula2.dao.UsuarioDao;
import br.edu.utfpr.pb.aula2.dao.VendaDao;
import br.edu.utfpr.pb.aula2.dao.VendaProdutoDao;
import br.edu.utfpr.pb.aula2.model.Compra;
import br.edu.utfpr.pb.aula2.model.CompraProduto;
import br.edu.utfpr.pb.aula2.model.CompraProdutoPK;
import br.edu.utfpr.pb.aula2.model.Permissao;
import br.edu.utfpr.pb.aula2.model.Usuario;
import br.edu.utfpr.pb.aula2.model.Venda;
import br.edu.utfpr.pb.aula2.model.VendaProduto;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainUsuario {

    public static void main(String[] args) {
        MainUsuario mu = new MainUsuario();
        
        System.exit(0);
    }

    public MainUsuario() {
        inserirPermissoes();
        
        inserirUsuarios();
        
        inserirVendaV1();
        
        inserirVendaV2();
        
        listarVendas();
        
        inserirCompra();
        
        listarCompras();
    }

    private void inserirPermissoes() {

        PermissaoDao permissaoDao = new PermissaoDao();

        try {

            Permissao p1 = new Permissao();
            p1.setNome("USER");
            
            Permissao p2 = new Permissao();
            p2.setNome("ADMIN");
            permissaoDao.insert(p2);
            
            Permissao p3 = new Permissao();
            p3.setNome("DEFAULT");
            permissaoDao.insert(p3);

        } catch (Exception e) {
            
            e.printStackTrace();
            System.out.println("Erro " + e.getMessage());
            e.printStackTrace();
        }

    }

    private void inserirUsuarios() {
        UsuarioDao usuarioDao = new UsuarioDao();
        PermissaoDao permissaoDao = new PermissaoDao();
        
        try {
            Usuario u1 = new Usuario();
            u1.setNome("João das Neves");
            u1.setEmail("jsnow@gmail.com");
            u1.setSenha("wwalker");
            u1.setAtivo(Boolean.TRUE);
            u1.setDataNascimento(LocalDate.of(2011,1,1));
            
            List<Permissao> permissoes = new ArrayList<>();
            permissoes.add(permissaoDao.findById(1L));
            permissoes.add(permissaoDao.findById(2L));
            
            u1.setPermissoes(permissoes);
            
            usuarioDao.insert(u1);            
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
    }
        private void atualizarUsuario(){
            UsuarioDao usuarioDao = new UsuarioDao();
            PermissaoDao permissaoDao = new PermissaoDao();
            
            try {
                Usuario u = usuarioDao.findById(1L);
                List<Permissao> permissoes = new ArrayList<>();
                permissoes.add(permissaoDao.findById(3L));
                
                u.setPermissoes(permissoes);
                
                usuarioDao.update(u);
            } catch (Exception e) {
            }
        }

    private void inserirVendaV1() {
        VendaDao vendaDao = new VendaDao();
        ProdutoDao produtoDao = new ProdutoDao();
        VendaProdutoDao vendaProdutoDao = new VendaProdutoDao();
        try {
            Venda v1 = new Venda();
            v1.setData(LocalDate.now());
            
            vendaDao.insert(v1);
            
            VendaProduto vp1 = new VendaProduto();
            vp1.setQuantidade(1);
            vp1.setValor(produtoDao.findById(1L).getValor());
            vp1.setProduto(produtoDao.findById(1L));
            vp1.setVenda(v1);
            
            vendaProdutoDao.insert(vp1);
            
            
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    private void inserirVendaV2() {
        VendaDao vendaDao = new VendaDao();
        ProdutoDao produtoDao = new ProdutoDao();
        try {
            Venda v1 = new Venda();
            v1.setData(LocalDate.now());
            
            List<VendaProduto> lista = new ArrayList<>();
            VendaProduto vp1 = new VendaProduto();
            vp1.setValor(produtoDao.findById(1L).getValor());
            vp1.setQuantidade(2);
            vp1.setProduto(produtoDao.findById(1L));
            vp1.setVenda(v1);
            
            //Add vendaproduto na lista
            lista.add(vp1);
            //VendaProduto 2
            
            VendaProduto vp2 = new VendaProduto();
            vp2.setValor(produtoDao.findById(1L).getValor());
            vp2.setQuantidade(4);
            vp2.setProduto(produtoDao.findById(2L));
            vp2.setVenda(v1);
            
            //Add vendaproduto na lista
            lista.add(vp2);
            
            v1.setVendaProdutos(lista);
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
    }

    private void listarVendas() {
        VendaDao vendaDao = new VendaDao();
        
        for (Venda v : vendaDao.findAll() ){
            System.out.println("Venda: " + v.getId() +  "- Data: " + v.getData() + " - Valor Total R$: " + v.getValorTotal());
            v.getVendaProdutos().forEach(vp -> System.out.println(vp.getProduto().getNome() + " - " + vp.getQuantidade() + " - " + vp.getValor()));
            
        }
        
    }

    private void inserirCompra() {
        CompraDao compraDao = new CompraDao();
        ProdutoDao produtoDao = new ProdutoDao();
        try {
            Compra c1 = new Compra();
            c1.setData(LocalDate.now());
            
            List<CompraProduto> lista = new ArrayList <> ();
            CompraProduto cp1 = new CompraProduto();
            cp1.setQuantidade(3);
            cp1.setValor(produtoDao.findById(1L).getValor());
            cp1.setId(new CompraProdutoPK(produtoDao.findById(1L), c1));
            lista.add (cp1);
            c1.setCompraProdutos(lista);
            compraDao.insert(c1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarCompras() {
         CompraDao compraDao = new CompraDao();
        
        for (Compra c : compraDao.findAll() ){
            System.out.println("Compra : " + c.getId() +  "- Data: " + c.getData() + " - Valor Total R$: " + c.getValorTotal());
            c.getCompraProdutos().forEach(cp -> System.out.println(cp.getQuantidade() + " - " + cp.getValor()));
            
        }
    }
    }


