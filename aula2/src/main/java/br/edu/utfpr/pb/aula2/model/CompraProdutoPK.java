package br.edu.utfpr.pb.aula2.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable //Pode ser injetado em outra entidade
public class CompraProdutoPK implements Serializable {
    
    @ManyToOne
    @JoinColumn (name = "produto_id", referencedColumnName = "id")
    private Produto produto;
    
    @ManyToOne
    @JoinColumn (name = "compra_id", referencedColumnName = "id")
    private Compra compra;

    public CompraProdutoPK() {
    }

    public CompraProdutoPK(Produto produto, Compra compra) {
        this.produto = produto;
        this.compra = compra;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.produto);
        hash = 37 * hash + Objects.hashCode(this.compra);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraProdutoPK other = (CompraProdutoPK) obj;
        if (!Objects.equals(this.produto, other.produto)) {
            return false;
        }
        if (!Objects.equals(this.compra, other.compra)) {
            return false;
        }
        return true;
    }

    
    
    
}
