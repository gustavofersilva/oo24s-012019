package br.edu.utfpr.pb.avaliacao.main;

import br.edu.utfpr.pb.avaliacao.dao.AlunoDao;
import br.edu.utfpr.pb.avaliacao.dao.ResponsavelDao;
import br.edu.utfpr.pb.avaliacao.model.Aluno;
import br.edu.utfpr.pb.avaliacao.model.Responsavel;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        
        Main main = new Main();
        System.exit(0);
    }

    public Main() {
        
        inserirAluno();
    
    }
   
  
    
 private void inserirAluno() {
        
        AlunoDao alunoDao = new AlunoDao();
        
        Aluno a1 = new Aluno();
        a1.setNome("Jose");
        a1.setCpf("4344");
        a1.setDatanascimento(LocalDate.EPOCH);
        a1.setTelefone("2321-2133");
        a1.setEmail("teste@teste.com");
        a1.setRa("12345");
        
        
        List<Responsavel> responsaveis = new ArrayList <>();
        Responsavel responsavel1 = new Responsavel();
        a1.setResponsaveis (responsaveis);
        responsavel1.setAluno(a1);
        responsavel1.setCpf("2222");
        responsavel1.setNome("Joao");
        responsavel1.setTelefone("3456-7899");
        responsaveis.add(responsavel1);
        
        Responsavel responsavel2 = new Responsavel();
        responsavel2.setAluno(a1);
        responsavel2.setCpf("2223");
        responsavel2.setNome("Maria");
        responsavel2.setTelefone("3456-7899");
        responsaveis.add(responsavel2);
        
        alunoDao.insert(a1);
        
    }

}
