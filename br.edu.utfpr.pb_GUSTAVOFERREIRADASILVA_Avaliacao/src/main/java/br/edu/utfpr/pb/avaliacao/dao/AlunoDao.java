package br.edu.utfpr.pb.avaliacao.dao;

import br.edu.utfpr.pb.avaliacao.model.Aluno;

public class AlunoDao extends GenericDao <Aluno, Integer> {

    public AlunoDao() {
        
        super(Aluno.class);
        
    }
    
    
    
}
