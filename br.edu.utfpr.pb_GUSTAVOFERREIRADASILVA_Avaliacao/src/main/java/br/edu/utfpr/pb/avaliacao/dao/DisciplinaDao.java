package br.edu.utfpr.pb.avaliacao.dao;

import br.edu.utfpr.pb.avaliacao.model.Disciplina;

public class DisciplinaDao extends GenericDao <Disciplina, Integer> {

    public DisciplinaDao() {
        super(Disciplina.class);
    }
    
    
    
}
