package br.edu.utfpr.pb.avaliacao.converter;

import java.time.LocalDate;
import java.sql.Date;
import javax.persistence.Converter;
import javax.persistence.AttributeConverter;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate value) {
        return (value == null ? null : Date.valueOf(value));

    }

    @Override
    public LocalDate convertToEntityAttribute(Date value) {
        
        return (value == null ? null : value.toLocalDate());
        
    }
    
    
    
}
