package br.edu.utfpr.pb.atitividade1.dao;

import br.edu.utfpr.pb.atividade1.model.Editora;

public class EditoraDao extends GenericDao <Editora, Integer>{
    
    public EditoraDao(){
        super(Editora.class);
    }
    
}
