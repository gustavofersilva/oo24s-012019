/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.pb.atitividade1.dao;

import br.edu.utfpr.pb.atividade1.model.Genero;

/**
 *
 * @author gustavo
 */
public class GeneroDao extends GenericDao <Genero, Integer> {
    
    public GeneroDao(){
        super(Genero.class);
    }
    
}
