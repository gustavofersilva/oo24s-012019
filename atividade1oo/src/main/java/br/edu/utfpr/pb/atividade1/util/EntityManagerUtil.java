package br.edu.utfpr.pb.atividade1.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EntityManagerUtil {

     public static EntityManagerFactory emf;
     
     public static EntityManager getEntityManager(){
         if (emf == null){
             emf = Persistence.createEntityManagerFactory("AtividadePU");
         }
         
         return emf.createEntityManager();
     }

    
}
