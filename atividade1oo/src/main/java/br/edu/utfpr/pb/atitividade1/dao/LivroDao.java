package br.edu.utfpr.pb.atitividade1.dao;

import br.edu.utfpr.pb.atividade1.model.Livro;

public class LivroDao extends GenericDao <Livro, Integer>{

    public LivroDao() {
        
        super(Livro.class);
    }
    
    
    
}
