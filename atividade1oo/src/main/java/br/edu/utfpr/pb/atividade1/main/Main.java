package br.edu.utfpr.pb.atividade1.main;

import br.edu.utfpr.pb.atitividade1.dao.AutorDao;
import br.edu.utfpr.pb.atitividade1.dao.CidadeDao;
import br.edu.utfpr.pb.atitividade1.dao.EditoraDao;
import br.edu.utfpr.pb.atitividade1.dao.GeneroDao;
import br.edu.utfpr.pb.atividade1.model.Autor;
import br.edu.utfpr.pb.atividade1.model.Cidade;
import br.edu.utfpr.pb.atividade1.model.Editora;
import br.edu.utfpr.pb.atividade1.model.Genero;


public class Main {
    public static void main(String[] args) {
        CidadeDao cidadeDao = new CidadeDao();
        
        Cidade cidade = new Cidade();
        cidade.setNome("Teste");
        
        cidadeDao.insert(cidade);
        
        System.out.println("\nLista de Cidades: ");
        cidadeDao.findAll().forEach(c -> System.out.println(c));
        
        
        AutorDao autorDao = new AutorDao();
        
        Autor autor = new Autor();
        autor.setNome("Machado de Assis");
        
        autorDao.insert(autor);
        
        System.out.println("\nLista de Autores: ");
        autorDao.findAll().forEach(a -> System.out.println(a));
        
        EditoraDao editoraDao = new EditoraDao();
        
        Editora editora = new Editora();
        editora.setNome("Brasiliense");
        
        editoraDao.insert(editora);
        
        System.out.println("\nLista de Editoras: ");
        editoraDao.findAll().forEach(e -> System.out.println(e));
        
        GeneroDao generoDao = new GeneroDao();
        
        Genero genero = new Genero();
        genero.setDescricao("Ficcao");
        
        generoDao.insert(genero);
        
        System.out.println("\nLista de Gêneros: ");
        editoraDao.findAll().forEach(g -> System.out.println(g));
        
        
        System.exit(0);
        
    }
}
