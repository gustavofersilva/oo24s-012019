package br.edu.utfpr.pb.aulaswing.model;

public enum EOperadora {
    VIVO, TIM, OI, CLARO;
}
