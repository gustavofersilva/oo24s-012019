package br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.dao;

import br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.model.Teste;

/**
 *
 * @author gustavo
 */

public class TesteDao extends GenericDao <Teste, Integer> {

    public TesteDao() {
        super(Teste.class);
    }
    
    
    
}
