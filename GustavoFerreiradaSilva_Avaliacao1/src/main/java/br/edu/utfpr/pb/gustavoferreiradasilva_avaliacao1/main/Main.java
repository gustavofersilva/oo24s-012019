package br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.main;

import br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.dao.TesteDao;
import br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.model.Teste;

public class Main {
 
    
    public static void main(String[] args) {
        TesteDao testeDao = new TesteDao();
        Teste teste = new Teste ();
        teste.setNome("Teste");
        
        testeDao.insert(teste);
        
        System.out.println(teste);
        System.out.println("\n Lista de Testes *******");
        testeDao.findAll().forEach(t -> System.out.println(t));
    }
    
}
