package br.edu.utfpr.pb.gustavoferreiradasilva_avaliacao1.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

@Convert
public class BooleanConverter implements AttributeConverter<Boolean, String>  {
    
    
    @Override
    public String convertToDatabaseColumn(Boolean value) {
        return (Boolean.TRUE.equals(value) ? "V" : "F"); //if aninhado
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        return "V".equals(value);
    }
    
}

